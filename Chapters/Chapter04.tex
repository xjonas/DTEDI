%************************************************
\chapter{Aim of the Work}\label{ch:aims} % $\mathbb{ZNR}$
%************************************************

\section{Objectives and Expected Results}

\subsection{Symbolic solver for quantified bit-vectors}
Throughout the rest of my PhD study, I plan to continue developing the
implemented symbolic \smt solver Q3B, which is aimed at solving
quantified bit-vector formulas. In particular, I plan to add support
for uninterpreted functions and the theory of arrays, which is highly
desirable for applications in the program verification. I also want to
implement extraction of an unsatisfiable core from the intermediate
\bdds that were produced during the computation.

Additionally, currently implemented approximations are very simple and
could benefit from better refinement in the case that the current
approximation is too coarse.

Moreover, I want to implement other variants of decision diagrams such
as zero-suppressed decision diagrams introduced by
Minato~\cite{Min93}, binary moment diagrams introduced by Bryant and
Chen~\cite{BC95}, and algebraic decision diagrams introduced by Bahar
et al.~\cite{BFGHMPS}. I plan to experimentally evaluate their effect
on the performance of the symbolic \smt solver for the quantified
bit-vectors.

\subsection{Hybrid approach to quantified bit-vectors}
Although results of the symbolic \smt solver for quantified
bit-vectors look promising, standard \smt solvers still perform better
on queries containing multiplication and complex
arithmetic. Therefore, I plan to develop a hybrid approach to \smt
solving of quantified bit-vector formulas that combines strengths of
both of these approaches. For example, parts of the quantified formula
that do not contain multiplication can be converted to the \bdd, which
can be used to guide the model search in the model-based quantifier
instantiation of the other parts of the formula. One possible way of
achieving this is adding support for \bdds to the solver \mcbv
developed by Zeljić et al. The \bdd representation can be added to the
currently used over-approximations by bit-patterns and arithmetic
intervals. Moreover, a tighter cooperation is possible -- if a value
of a variable is decided during the \mcbv computation, it can be used
to partially instantiate a quantified part of the formula, for which
the \bdd will be computed.

\subsection{Unconstrained variable propagation for quantified
  bit-vectors}
Simplifications using unconstrained variables can be extended to
quantified formulas. However, in the quantified setting, constraints
among variables can be introduced also by the order in which the
variables are quantified. We have formulated a hypothesis that
describes a sufficient condition for the quantified variable to be
considered as unconstrained. Based on this hypothesis and a partial
proof of its validity, we have implemented a proof-of-concept
simplification procedure that can simplify quantified formulas that
contain unconstrained variables. Although the initial experimental
results, conducted on the formulas from the semi-symbolic model
checker \SymDivine, look promising, the formal proof of the
correctness is not yet complete.

Moreover, the simplifications of formulas with unconstrained variables
can be further extended. For example, if a formula contains a term
$k \times x$ in which the variable $x$ is unconstrained, this term can
be replaced by a simpler term regardless the parity of the value
$k$. In particular, if $i$ is the largest number for which $2^i$
divides the constant $k$ and the unconstrained variable $x$ has
bit-width $n$, the term $k \times x$ can be rewritten to
$extract_0^{n-i}(x) \cdot 0^i$. Intuitively, the term $k \times x$ can
have every possible bit-vector value that has the last significant $i$
bits zero. This approach can be also extended to the multiplication of
two variables from which one is unconstrained. I plan to investigate
further extensions to the terms containing division and remainder
operations and to publish a paper concerning propagation of
unconstrained variables in quantified formulas and extensions of
unconstrained variable simplification to multiplication and
division. I will also experimentally evaluate the effect of such
simplifications on our solver Q3B and on state-of the art solvers such
as Boolector, CVC4, and Z3.

\subsection{Complexity of BV2}
As was explained in Section \ref{sec:complexity}, the precise
complexity of quantified bit-vector formulas with binary-encoded
bit-widths and without uninterpreted functions is not known. It is
known to be in \EXPSPACE and to be \NEXPTIME-hard. However, a class
for which the problem is complete is not known.

According to our investigation, it is probably complete for neither of
those complexity classes. We are working on a proof which shows that
BV2 is complete for the class of problems solvable by the
\emph{alternating Turing machine} (\atm) with the exponential space
and \emph{polynomial number of alternations} with respect to log-space
reductions. This class is usually denoted as \AEXPTIMEp and is known
to be in between \NEXPTIME and \EXPSPACE~\cite{HKVV15,
  Luc16}. However, whether any of the inclusions is proper is not
known.

\section{Progression Schedule}
The plan of my future study and research activities is following:

\begin{description}[style=nextline,leftmargin=0.8cm]
\item [now -- January 2019] Improvements and maintaining of the
  developed symbolic \smt solver Q3B.
\item [now -- January 2017] Extending unconstrained variable
  propagation to quantified formulas and to non-linear multiplication
  and division.
\item [January 2017] Doctoral exam and defense of this thesis proposal.
\item [February 2017 -- April 2017] Proving a precise complexity class
  of the quantified bit-vector formulas without uninterpreted functions
  and with binary encoded bit-widths. Preparing a paper on this topic.
\item [May 2017 -- December 2017] Development of a hybrid approach to
  \smt solving of quantified bit-vector formulas combining symbolic
  representation of formulas with \dpllt or \mcsat.
\item [January 2018 -- August 2018] Implementing a hybrid \smt solver
\item [September 2018 -- January 2019] Text of the PhD thesis.
\item [January 2019] The final version of the thesis.
\end{description}

%*****************************************
%*****************************************
%*****************************************
%*****************************************
%*****************************************

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ClassicThesis"
%%% End:
