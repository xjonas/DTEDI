%************************************************
\chapter{Introduction}\label{ch:introduction}
% ************************************************

During the last decades, the area of \emph{satisfiability modulo
  theories} (\smt) has undergone steep development in both theory and
practice. Achieved advances of \smt solving opened new research
directions in program analysis and verification, where \smt solvers
are now seen as standard tools.

The task for an \smt solver is to decide for a given first-order
formula in a given first-order theory whether the formula is
satisfiable. Usually, if the formula is satisfiable, the \smt solver
also has the ability to provide its model. Modern \smt solvers support
wide range of different first-order theories -- for example theories
of integers, real numbers, floating-point numbers, arrays, strings,
inductively defined data types, bit-vectors, and various combinations
and fragments of formulas over these theories~\cite{BFT15, ZZG13,
  RB16}. From the software analysis and verification point of view, a
particularly important of these theories is the theory of bit-vectors,
which can be used to describe properties of computer programs, since
programs usually use data-types of bounded size instead of
mathematical integers. Additionally, operations over these bounded
data-types naturally correspond to operations of the bit-vector theory.

The benefit of describing properties of programs by bit-vector
formulas is twofold. Formulas in the bit-vector theory allow to model
the program's behavior precisely including possible arithmetic
overflows and underflows. Furthermore, in contrast to the theory of
integers, the satisfiability of the bit-vector theory is decidable
even if the multiplication is allowed.

Therefore, quantifier-free bit-vector formulas are used in tools for
symbolic execution, bounded model checking, analysis of hardware
circuits, static analysis, or test generation~\cite{CGPD08, CDE08,
  Lei10, CFM12}. Most of the current \smt solvers for the
quantifier-free bit-vector formulas eagerly or lazily translate the
formula to the propositional logic (\emph{bit-blasting}) and use an
efficient \sat solver to decide its satisfiability. Therefore, the
efficiency of most of the bit-vector \smt solvers is tightly connected
to the efficiency of the \sat solvers. Plenty of solvers for the
quantifier-free bit-vector formulas exist: Beaver~\cite{Beaver},
Boolector~\cite{Boolector}, CVC4~\cite{CVC4}, MathSAT5~\cite{MathSAT},
OpenSMT~\cite{OpenSMT}, Sonolar~\cite{Sonolar}, Spear~\cite{Spear},
STP~\cite{STP}, UCLID~\cite{LS04}, Yices~\cite{Yices}, and
Z3~\cite{Z3}.

In some cases, quantifier-free formulas are not succinct enough and
using quantification is necessary to keep the size of the formula
reasonable. Quantified bit-vector formulas arise naturally for example
in applications that need to decide equality of two symbolically
represented states~\cite{BHB14}, or in applications that generate loop
invariants, ranking functions, or loop
summaries~\cite{WHD13,KLW13}. However, the current \smt solvers'
support of quantified bit-vector logic is modest -- only CVC4, Yices,
and Z3 officially support quantifiers in bit-vector
formulas. Recently, quantifiers have been also implemented in an
development version of Boolector~\cite{BoolectorComp}. All of these
\smt solvers solve quantified bit-vector formulas by some variant of
quantifier-instantiation and using a solver for quantifier-free
formulas as an oracle.

In the last year, we have proposed a different approach. We have
implemented a symbolic \smt solver Q3B, which is based on binary
decision diagrams. We have shown that it can not only compete with
state-of-the art \smt solvers, but outperforms them in many
cases~\cite{JS16}. However, \bdds are not a silver bullet; the
symbolic \smt solver fails on formulas containing non-trivial
multiplication and complex arithmetic. Therefore, as the aim of my PhD
study, I plan to develop a hybrid approach to solving quantified
bit-vectors, which combines symbolic representation by \bdds with
search techniques employed by existing state-of-the-art solvers. I
also plan to continue developing the symbolic solver Q3B itself,
namely improve its efficiency and add support for features as
uninterpreted functions and arrays, which are important in the
software verification.

The thesis proposal is organized as follows. Chapter~\ref{ch:sota}
summarizes the state of the art and is divided into six
sections. Section~\ref{sec:prelim} introduces necessary background and
notations from the propositional and first-order
logic. Section~\ref{sec:sat} describes approaches to solving
propositional satisfiability and Section~\ref{sec:smt} describes
approaches to solving satisfiability modulo theories. Sections
\ref{sec:qfbv} and \ref{sec:bv} are devoted to solving quantifier-free
and quantified formulas over the theory of bit-vectors,
respectively. Finally, Section~\ref{sec:complexity} section describes
results concerning the computational complexity of bit-vector
logics. Chapter~\ref{ch:achieved} describes results that we have
achieved during the first two years of my PhD
study. Chapter~\ref{ch:aims} presents the aim of the thesis and states
plans for the remaining part of my PhD study.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../ClassicThesis"
%%% End:
